import { Chapter } from "../../../src";
import types01 from "./01_types";

export default new Chapter("Basics").addUnits(types01);
