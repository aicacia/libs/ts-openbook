import { Unit } from "../../../../src";
import definitions01 from "./01_definitions";

export default new Unit("Types").addLessons(definitions01);
