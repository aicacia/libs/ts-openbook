import { Definition } from "../../../../../src";

export const definition = new Definition("Definition").addChildren(
  "Is a statement of the meaning of a term"
);

export const example = new Definition("Example").addChildren(
  "Sample to explain an aspect of a ",
  definition.getReference()
);

export const lesson = new Definition("Lesson").addChildren(
  "Content to learn one or more ",
  definition.getReference(),
  "(s)"
);
