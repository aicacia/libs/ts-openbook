import { Code, Example } from "../../../../../src";
import { definition } from "./definitions";

export const creatingADefinition = new Example(
  "Creating a Definition"
).addChildren(
  "A ",
  definition.getReference(),
  " can be create like this this",
  new Code(`
  import { Definition } from "@aicacia/openbook";

  export const definition = new Definition("Definition").addChildren(
    "Is a statement of the meaning of a term"
  );  
  `)
);
