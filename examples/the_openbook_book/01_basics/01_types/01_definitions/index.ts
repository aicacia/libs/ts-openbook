import { Lesson } from "../../../../../src";
import { definition, example, lesson } from "./definitions";
import { creatingADefinition } from "./examples";

export default new Lesson("Definitions").addChildren(
  definition.getReference(),
  "s are to define a term, (see ",
  creatingADefinition.getReference(),
  ")",
  example.getReference(),
  "s are for explaing an aspect of a ",
  definition.getReference(),
  ", and ",
  lesson.getReference(),
  "s are used to learn information"
);
