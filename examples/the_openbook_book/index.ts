import { Author, Book } from "../../src";
import basics from "./01_basics";

export default new Book("The OpenBook Book")
  .addAuthors(new Author("Nathan Faucett").setEmail("nathanfaucett@gmail.com"))
  .addChapters(basics);
