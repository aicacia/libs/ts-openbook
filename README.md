# ts-openbook

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-openbook/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/openbook)](https://www.npmjs.com/package/@aicacia/openbook)
[![pipelines](https://gitlab.com/aicacia/libs/ts-openbook/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-openbook/-/pipelines)

aicacia openbook
