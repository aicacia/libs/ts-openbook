import { AbstractInput } from "./Question";

export class Input extends AbstractInput<string> {
  private type = "raw";
  private checker: (answer: string) => Promise<[number, number]>;

  constructor(checker: (answer: string) => Promise<[number, number]>) {
    super();
    this.checker = checker;
  }

  setType(type: string) {
    this.type = type;
    return this;
  }
  getType() {
    return this.type;
  }

  getChecker() {
    return this.checker;
  }
  setChecker(checker: (answer: string) => Promise<[number, number]>) {
    this.checker = checker;
    return this;
  }

  check(answer: string): Promise<[number, number]> {
    return this.checker(answer);
  }
}
