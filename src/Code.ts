import { none, Option } from "@aicacia/core";
import { Text } from "./Text";

export class Code extends Text {
  private language = "txt";
  private inline = false;
  private filename: Option<string> = none();

  constructor(
    value: string,
    language = "txt",
    inline = false,
    filename?: string
  ) {
    super(value);
    this.language = language;
    this.inline = inline;
    if (filename) {
      this.filename.replace(filename);
    }
  }

  setLanguage(language: string) {
    this.language = language;
    return this;
  }
  getLanguage() {
    return this.language;
  }

  setInline(inline: boolean) {
    this.inline = inline;
    return this;
  }
  getInline() {
    return this.inline;
  }

  setFilename(filename: string) {
    this.filename.replace(filename);
    return this;
  }
  getFilename() {
    return this.filename;
  }
}
