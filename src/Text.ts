import { Reference } from "./Reference";
import { Node } from "./Node";

export class Text extends Node {
  private value: string;

  constructor(value = "") {
    super();
    this.value = value;
  }

  getReference(): Reference {
    return new Reference(this, this);
  }

  setValue(value: string) {
    this.value = value;
    return this;
  }
  getValue() {
    return this.value;
  }
}
