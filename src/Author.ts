import { none, Option } from "@aicacia/core";
import { Node } from "./Node";
import { Text } from "./Text";

export class Author {
  private name: Node;
  private email: Option<string> = none();
  private phone: Option<string> = none();

  constructor(name: Node | string) {
    this.name = typeof name === "string" ? new Text(name) : name;
  }

  setName(name: Node) {
    this.name = name;
    return this;
  }
  getName<T extends Node = Node>(): T {
    return this.name as T;
  }

  setEmail(email: string) {
    this.email.replace(email);
    return this;
  }
  getEmail() {
    return this.email;
  }

  setPhone(phone: string) {
    this.phone.replace(phone);
    return this;
  }
  getPhone() {
    return this.phone;
  }
}
