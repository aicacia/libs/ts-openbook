import { none, Option } from "@aicacia/core";
import type { Chapter } from "./Chapter";
import type { Lesson } from "./Lesson";
import { Reference } from "./Reference";
import type { Node } from "./Node";
import { Text } from "./Text";

export class Unit {
  private chapterIndex = 0;
  private chapter: Option<Chapter> = none();
  private title: Node;
  private lessons: Lesson[] = [];

  constructor(title: Node | string) {
    this.title = typeof title === "string" ? new Text(title) : title;
  }

  getId() {
    return this.chapter
      .map((chapter) => `${chapter.getId()}.${this.chapterIndex + 1}`)
      .unwrapOr((this.chapterIndex + 1).toString());
  }
  setTitle(title: Node) {
    this.title = title;
    return this;
  }
  getTitle<T extends Node = Node>() {
    return this.title as T;
  }

  getLesson(index: number): Option<Lesson> {
    return Option.from(this.lessons[index]);
  }
  getLessons(): ReadonlyArray<Lesson> {
    return this.lessons;
  }
  addLessons(...lessons: ReadonlyArray<Lesson>) {
    lessons.forEach((lesson) => {
      lesson.UNSAFE_setUnit(this, this.lessons.length);
      this.lessons.push(lesson);
    });
    return this;
  }

  getReference() {
    return new Reference(this, this.title);
  }

  /**
   * @ignore
   */
  UNSAFE_setChapter(chapter: Chapter, chapterIndex: number) {
    this.chapterIndex = chapterIndex;
    this.chapter.replace(chapter);
    return this;
  }
}
