import { Reference } from "./Reference";
import { Node } from "./Node";
import type { Unit } from "./Unit";
import { none, Option } from "@aicacia/core";
import { Book } from "./Book";
import { Text } from "./Text";

export class Chapter {
  private bookIndex = 0;
  private book: Option<Book> = none();
  private title: Node;
  private units: Unit[] = [];

  constructor(title: Node | string) {
    this.title = typeof title === "string" ? new Text(title) : title;
  }

  setTitle(title: Node) {
    this.title = title;
    return this;
  }
  getTitle<T extends Node = Node>(): T {
    return this.title as T;
  }

  getId() {
    return (this.bookIndex + 1).toString();
  }

  getUnit(index: number): Option<Unit> {
    return Option.from(this.units[index]);
  }
  getUnits(): ReadonlyArray<Unit> {
    return this.units;
  }
  addUnits(...units: ReadonlyArray<Unit>) {
    units.forEach((unit) => {
      unit.UNSAFE_setChapter(this, this.units.length);
      this.units.push(unit);
    });
    return this;
  }

  getReference() {
    return new Reference(this, this.title);
  }

  /**
   * @ignore
   */
  UNSAFE_setBook(book: Book, bookIndex: number) {
    this.book.replace(book);
    this.bookIndex = bookIndex;
    return this;
  }
}
