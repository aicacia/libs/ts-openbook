import { Node } from "./Node";

export interface INodeRoot {
  onAddNode(node: Node): this;
}
