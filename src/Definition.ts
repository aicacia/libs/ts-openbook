import { none, Option } from "@aicacia/core";
import { Node } from "./Node";
import { Reference } from "./Reference";
import { Text } from "./Text";

export class Definition implements INodeRoot {
  private lessonIndex = 0;
  private lesson: Option<Lesson> = none();
  private title: Node;
  private childCount = 0;
  private children: Node[] = [];
  private references: Set<unknown> = new Set();

  constructor(title: Node | string) {
    this.title = typeof title === "string" ? new Text(title) : title;
  }

  setTitle(title: Node) {
    this.title = title;
    return this;
  }
  getTitle<T extends Node = Node>(): T {
    return this.title as T;
  }

  getId() {
    return this.lesson
      .map((lesson) => `${lesson.getId()}.${this.lessonIndex + 1}`)
      .unwrapOr((this.lessonIndex + 1).toString());
  }

  getReferences() {
    return this.references;
  }
  getReference() {
    return new Reference(this, this.title);
  }

  getChildren(): ReadonlyArray<Node> {
    return this.children;
  }
  addChildren(...children: ReadonlyArray<Node | string>) {
    children.forEach((child) => {
      const node = typeof child === "string" ? new Text(child) : child;
      this.children.push(node);
      this.onAddNode(node);
    });
    return this;
  }

  onAddNode(node: Node) {
    if (node instanceof Reference) {
      this.references.add(node.getValue());
    }
    node.UNSAFE_setRoot(this, this.childCount++);
    return this;
  }

  /**
   * @ignore
   */
  UNSAFE_setLesson(lesson: Lesson, lessonIndex: number) {
    this.lessonIndex = lessonIndex;
    this.lesson.replace(lesson);
    return this;
  }
}

import { Lesson } from "./Lesson";
import { INodeRoot } from "./INodeRoot";
