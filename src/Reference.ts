import { Node } from "./Node";
import { Text } from "./Text";

export class Reference<T = unknown> extends Node {
  private value: T;
  private node: Node;

  constructor(value: T, node: Node | string) {
    super();
    this.value = value;
    this.node = typeof node === "string" ? new Text(node) : node;
  }

  getValue(): T {
    return this.value;
  }
  getNode() {
    return this.node;
  }
}
