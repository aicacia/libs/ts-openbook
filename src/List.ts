import type { INodeRoot } from "./INodeRoot";
import { Node } from "./Node";

export class List extends Node {
  private ordered = false;
  private items: Node[] = [];

  setOrdered(ordered = true) {
    this.ordered = ordered;
    return this;
  }
  getOrdered() {
    return this.ordered;
  }

  addItem(...items: Node[]) {
    return this.addItems(items);
  }
  addItems(items: Node[]) {
    this.items.push(...items);
    return this;
  }
  getItems() {
    return this.items;
  }

  protected onSetRoot(root: INodeRoot) {
    this.items.forEach((child) => root.onAddNode(child));
    return this;
  }
}
