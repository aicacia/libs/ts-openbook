import { none, Option } from "@aicacia/core";
import type { Node } from "./Node";
import { Reference } from "./Reference";
import { Text } from "./Text";
import type { Unit } from "./Unit";

export class Lesson implements INodeRoot {
  private title: Node;
  private unitIndex = 0;
  private unit: Option<Unit> = none();
  private childCount = 0;
  private children: Node[] = [];
  private definitions: Set<Definition> = new Set();
  private examples: Set<Example> = new Set();

  constructor(title: Node | string) {
    this.title = typeof title === "string" ? new Text(title) : title;
  }

  setTitle(title: Node) {
    this.title = title;
    return this;
  }
  getTitle<T extends Node = Node>(): T {
    return this.title as T;
  }

  getId() {
    return this.unit
      .map((unit) => `${unit.getId()}.${this.unitIndex + 1}`)
      .unwrapOr((this.unitIndex + 1).toString());
  }
  getUnit() {
    return this.unit;
  }

  getDefinitions() {
    return this.definitions;
  }
  getExamples() {
    return this.examples;
  }

  getChildren(): ReadonlyArray<Node> {
    return this.children;
  }
  addChildren(...children: ReadonlyArray<Node | string>) {
    children.forEach((child) => {
      const node = typeof child === "string" ? new Text(child) : child;
      this.children.push(node);
      this.onAddNode(node);
    });
    return this;
  }

  onAddNode(node: Node) {
    if (node instanceof Reference) {
      this.addReference(node.getValue());
    }
    node.UNSAFE_setRoot(this, this.childCount++);
    return this;
  }

  private addReference(reference: unknown) {
    if (reference instanceof Definition && !this.definitions.has(reference)) {
      this.definitions.add(reference);
      reference.UNSAFE_setLesson(this, this.definitions.size);

      for (const ref of reference.getReferences()) {
        this.addReference(ref);
      }
    } else if (reference instanceof Example && !this.examples.has(reference)) {
      this.examples.add(reference);
      reference.UNSAFE_setLesson(this, this.examples.size);

      for (const ref of reference.getReferences()) {
        this.addReference(ref);
      }
    }
    return this;
  }

  /**
   * @ignore
   */
  UNSAFE_setUnit(unit: Unit, unitIndex: number) {
    this.unitIndex = unitIndex;
    this.unit.replace(unit);
    return this;
  }
}

import { Definition } from "./Definition";
import { Example } from "./Example";
import { INodeRoot } from "./INodeRoot";
