import { INodeRoot } from "./INodeRoot";
import { Node } from "./Node";
import { Text } from "./Text";

export class Block extends Node {
  private children: Node[] = [];

  getChildren(): ReadonlyArray<Node> {
    return this.children;
  }
  addChildren(...children: ReadonlyArray<Node | string>) {
    children.forEach((child) => {
      const node = typeof child === "string" ? new Text(child) : child;
      this.children.push(node);
    });
    return this;
  }

  protected onSetRoot(root: INodeRoot) {
    this.children.forEach((child) => root.onAddNode(child));
    return this;
  }
}
