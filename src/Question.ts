import { none, Option } from "@aicacia/core";
import type { Rng } from "@aicacia/rand";
import { Node } from "./Node";
export abstract class AbstractInput<T = any> extends Node {
  abstract check(answer: T): Promise<[number, number]>;
}

export class Question<T = any> extends Node {
  protected prompt: Node;
  protected input: AbstractInput<T>;
  protected explanation: Option<Node> = none();

  constructor(prompt: Node, input: AbstractInput, explanation?: Node) {
    super();
    this.prompt = prompt;
    this.input = input;
    if (explanation) {
      this.explanation.replace(explanation);
    }
  }

  check(answer: T) {
    return this.input.check(answer);
  }

  getPrompt() {
    return this.prompt;
  }
  getInput() {
    return this.input;
  }
  getExplanation() {
    return this.explanation;
  }
}

export type IQuestionGenerator<T = any> = (rng: Rng) => Question<T>;

export class QuestionGenerator<T = any> extends Node {
  private generator: IQuestionGenerator<T>;

  constructor(generator: IQuestionGenerator<T>) {
    super();
    this.generator = generator;
  }

  generate(rng: Rng): Question<T> {
    return this.generator(rng);
  }
}
