import type { Author } from "./Author";
import type { Chapter } from "./Chapter";
import { Reference } from "./Reference";
import { Node } from "./Node";
import { Text } from "./Text";
import { Option } from "@aicacia/core";

export class Book {
  private title: Node;
  private authors: Author[] = [];
  private chapters: Chapter[] = [];

  constructor(title: Node | string) {
    this.title = typeof title === "string" ? new Text(title) : title;
  }

  setTitle(title: Node) {
    this.title = title;
    return this;
  }
  getTitle<T extends Node = Node>(): T {
    return this.title as T;
  }

  getAuthors(): ReadonlyArray<Author> {
    return this.authors;
  }
  addAuthors(...authors: ReadonlyArray<Author>) {
    authors.forEach((author) => {
      this.authors.push(author);
    });
    return this;
  }

  getChapter(index: number): Option<Chapter> {
    return Option.from(this.chapters[index]);
  }
  getChapters(): ReadonlyArray<Chapter> {
    return this.chapters;
  }
  addChapters(...chapters: ReadonlyArray<Chapter>) {
    chapters.forEach((chapter) => {
      chapter.UNSAFE_setBook(this, this.chapters.length);
      this.chapters.push(chapter);
    });
    return this;
  }

  getReference() {
    return new Reference(this, this.title);
  }
}
