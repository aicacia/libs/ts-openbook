import { none, Option } from "@aicacia/core";
import type { Definition } from "./Definition";
import type { Example } from "./Example";
import type { Lesson } from "./Lesson";

export abstract class Node {
  protected nodeIndex = 0;
  protected root: Option<Lesson | Definition | Example> = none();

  protected onSetRoot(_root: Lesson | Definition | Example) {
    return this;
  }

  /**
   * @ignore
   */
  UNSAFE_setRoot(root: Lesson | Definition | Example, nodeIndex: number) {
    if (this.root.isSome()) {
      throw new Error(
        `Trying to add ${this} to ${root} but it is already a child of ${this.root.unwrap()}`
      );
    }
    this.nodeIndex = nodeIndex;
    this.root.replace(root);
    this.onSetRoot(root);
    return this;
  }
}
