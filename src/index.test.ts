import * as tape from "tape";
import book from "../examples/the_openbook_book";
import { Text } from "./Text";

tape("book", (assert: tape.Test) => {
  assert.equal(book.getTitle<Text>().getValue(), "The OpenBook Book");

  const chapter = book.getChapter(0).unwrap();
  assert.equal(chapter.getTitle<Text>().getValue(), "Basics");

  const unit = chapter.getUnit(0).unwrap();
  assert.equal(unit.getTitle<Text>().getValue(), "Types");

  const lesson = unit.getLesson(0).unwrap();
  assert.equal(lesson.getTitle<Text>().getValue(), "Definitions");
  assert.equal(lesson.getDefinitions().size, 3);
  assert.equal(lesson.getExamples().size, 1);

  assert.end();
});
