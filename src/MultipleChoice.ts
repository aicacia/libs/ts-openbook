import { Block } from "./Block";
import { AbstractInput } from "./Question";

export class Choice extends Block {
  private key = "";
  private correct = false;

  getKey() {
    return this.key;
  }
  isCorrect() {
    return this.correct;
  }

  /**
   * @ignore
   */
  UNSAFE_setKey(key: string) {
    this.key = key;
    return this;
  }
}

export class MultipleChoice extends AbstractInput<string[]> {
  protected allOrNothing = false;
  protected choices: Choice[] = [];

  hasMultipleAnswers() {
    return this.choices.filter((choice) => choice.isCorrect()).length > 1;
  }
  getChoices(): ReadonlyArray<Choice> {
    return this.choices;
  }

  check(answer: string[]): Promise<[number, number]> {
    const correctChoices = this.choices.filter((choice) => choice.isCorrect());

    const correct = correctChoices.reduce(
      (correct, choice) =>
        answer.includes(choice.getKey()) ? correct + 1 : correct,
      0
    );

    if (this.allOrNothing && correct !== correctChoices.length) {
      return Promise.resolve([0, correctChoices.length]);
    } else {
      return Promise.resolve([correct, correctChoices.length]);
    }
  }

  addChoice(...choices: ReadonlyArray<Choice>) {
    return this.addChoices(choices);
  }
  addChoices(choices: ReadonlyArray<Choice>) {
    choices.forEach((choice) => {
      choice.UNSAFE_setKey(this.choices.length.toString(36));
      this.choices.push(choice);
    });
    return this;
  }
}
