import { INodeRoot } from "./INodeRoot";
import { Node } from "./Node";

export class Table extends Node {
  private headers: Node[] = [];
  private rows: Node[][] = [];

  addHeaders(...headers: Node[]) {
    headers.forEach((header) => {
      this.headers.push(header);
    });
    return this;
  }
  getHeaders() {
    return this.headers;
  }

  addRows(...rows: Node[][]) {
    rows.forEach((row) => {
      if (row.length !== this.headers.length) {
        throw new Error(
          `Invalid row length ${row.length} != ${this.headers.length} header(s)`
        );
      } else {
        this.rows.push(row);
      }
    });
    return this;
  }
  getRows() {
    return this.rows;
  }

  protected onSetRoot(root: INodeRoot) {
    this.headers.forEach((header) => root.onAddNode(header));
    this.rows.forEach((row) => row.forEach((node) => root.onAddNode(node)));
    return this;
  }
}
