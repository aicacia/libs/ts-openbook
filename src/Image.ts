import { Reference } from "./Reference";
import { Node } from "./Node";
export class Image extends Node {
  private label: Node;
  private source: string;

  constructor(source: string, label: Node) {
    super();
    this.source = source;
    this.label = label;
  }

  setSource(source: string) {
    this.source = source;
    return this;
  }
  getSource() {
    return this.source;
  }

  setLabel(label: Node) {
    this.label = label;
    return this;
  }
  getLabel() {
    return this.label;
  }

  getReference() {
    return new Reference(this, this.label);
  }
}
